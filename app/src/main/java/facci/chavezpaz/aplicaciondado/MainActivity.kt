package facci.chavezpaz.aplicaciondado

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


/**
 * Esta actividad permite al usuario tirar un dado y ver el resultado
 * en la pantalla.
 */
class MainActivity : AppCompatActivity() {
    class Dice(val numSides: Int) {

        fun roll(): Int {
            return (1..numSides).random()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton: Button = findViewById(R.id.button)

        rollButton.setOnClickListener {
            //val toast = Toast.makeText(this, "Dado lanzado!", Toast.LENGTH_SHORT)
            //toast.show()
            //val resultTextView: TextView = findViewById(R.id.textView)
            //resultTextView.text = "6"
            rollDice()
        }
    }

    /**
     * Tira los dados y actualiza la pantalla con el resultado.
     */
    private fun rollDice() {

        // Crea un nuevo objeto Dice con 6 lados y hazlo rodar
        val dice = Dice(6)
        val diceRoll = dice.roll()
        // Actualizar la pantalla con la tirada de dados
        val resultTextView: TextView = findViewById(R.id.textView)
        resultTextView.text = diceRoll.toString()
    }


}